# Panic utility library

## Introduction

The `panic` library provides the `panic` utility function for aborting a C
program with a message and its source code location.

## Motivation

Most non-trivial C programs have preconditions or invariants that cannot be
checked by the compiler.
When feasible, these can be checked at runtime, but requires aborting or exiting
the program on violations.
Some programs define their own panic function instead of calling `abort()` or
`exit()` directly, but the custom functions are necessarily very similar.
The `panic` library provides a reusable `panic()` function.

## Dependencies

The `panic` library requires the header file `ecb.h` from
http://software.schmorp.de/pkg/libecb.html to be installed somewhere where the
compiler can find it.

## Installation

This library can be installed with the following commands

```bash
meson build
ninja -C build
sudo ninja -C build install
```

You can then get the compiler flags and libraries through `pkg-config` with
`pkg-config --cflags panic-${X}` and `pkg-config --libs panic-${X}` with `${X}`
substituted by the major version of this library.

## Documentation

The API is documented in comments in the `panic.h` header file.
