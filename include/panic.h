/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * panic.h
 * -------
 * This header provides the panic(...) function-like macro for aborting the
 * program with a message.
 */

#ifndef PANIC_H
#define PANIC_H

#include <ecb.h>

/* Internal implementation of panic. Do not call this function directly. Use the
 * panic(...) macro instead. */
ecb_cold ecb_attribute((format(printf, 4, 5))) ecb_noreturn
        void panic_impl(const char *file,
                        unsigned line,
                        const char *function,
                        const char *format,
                        ...);

/**
 * Aborts the program with a message together with the source code location of
 * the caller.
 *
 * The first argument to this macro is the format and the remaining arguments
 * are the format arguments. For example:
 *
 *   panic("foo should have been 0 here, but was %d.", foo);
 *
 * This function-like macro does not return.
 */
#define panic(...) panic_impl(__FILE__, __LINE__, __func__, __VA_ARGS__)

/* Internal implementation only meant for panic_check. Do not call this function
 * directly. */
ecb_cold ecb_noreturn void panic_check_fail(const char *file,
                                            unsigned line,
                                            const char *function,
                                            const char *condition);

/**
 * Checks that the given condition is true, or calls panic otherwise.
 *
 * This macro behaves much like assert, but the check is always performed, even
 * in release builds.
 */
#define panic_check(cond) \
    do { \
        if (cond) { \
            /* empty */ \
        } else { \
            panic_check_fail(__FILE__, __LINE__, __func__, #cond); \
        } \
    } while (0)

#endif /* !PANIC_H */
