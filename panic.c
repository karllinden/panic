/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <panic.h>

void panic_impl(const char *file,
                unsigned line,
                const char *function,
                const char *format,
                ...) {
    fprintf(stderr, "panic: %s:%u: %s: ", file, line, function);

    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    fputc('\n', stderr);
    fflush(stderr);

    abort();
}

void panic_check_fail(const char *file,
                      unsigned line,
                      const char *function,
                      const char *condition) {
    panic_impl(file, line, function, "Check `%s' failed.", condition);
}
