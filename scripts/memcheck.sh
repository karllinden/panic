#!/bin/bash

set -e

print_usage_and_exit() {
    echo "Usage: memcheck.sh" 1>&2
    exit 1
}

if [[ $# -ne 0 ]]
then
    print_usage_and_exit
fi

wrapper="valgrind --log-file=valgrind/memcheck-%p.log --leak-check=yes"

rm -rf build
meson --buildtype=debugoptimized build
cd build
ninja -v

ulimit -c 0
mkdir valgrind
meson test \
    --print-errorlogs \
    --num-processes=1 \
    --wrapper="$wrapper"

logs=$(find valgrind -type f)

set +e

status=0
for log in $logs
do
    grep -q 'ERROR SUMMARY: 0 errors from 0 contexts' $log
    s=$?
    status=$((status + $s))
    if [[ $s -ne 0 ]];
    then
        echo
        echo $log
        cat $log
    fi
done

if [[ $status -ne 0 ]]
then
    echo
    cat meson-logs/testlog-valgrind.txt
fi

exit $status
