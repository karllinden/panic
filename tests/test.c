/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>

#include <ecb.h>

#include "test.h"

static int can_have_raise_signal_tests = 0;

static Suite *suite;

static const char *determine_suite_name(const char *arg) {
    const char *s = strrchr(arg, '/');
    return s != NULL ? s + 1 : arg;
}

static TCase *create_test(const char *name) {
    TCase *tc = tcase_create(name);
    tcase_add_checked_fixture(tc, set_up, tear_down);
    suite_add_tcase(suite, tc);
    return tc;
}

void add_test(const char *name, TFun func) {
    TCase *tc = create_test(name);
    tcase_add_test(tc, func);
}

void add_loop_test(const char *name, TFun func, int start, int end) {
    TCase *tc = create_test(name);
    tcase_add_loop_test(tc, func, start, end);
}

void add_raise_signal_test(const char *name, TFun func, int signal) {
    if (can_have_raise_signal_tests) {
        TCase *tc = create_test(name);
        tcase_add_test_raise_signal(tc, func, signal);
    }
}

int main(ecb_unused int argc, const char *argv[]) {
    suite = suite_create(determine_suite_name(argv[0]));
    SRunner *srunner = srunner_create(suite);
    can_have_raise_signal_tests = (srunner_fork_status(srunner) == CK_FORK);

    add_tests();

    srunner_run_all(srunner, CK_NORMAL);
    int n_failed = srunner_ntests_failed(srunner);
    srunner_free(srunner);

    return n_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
